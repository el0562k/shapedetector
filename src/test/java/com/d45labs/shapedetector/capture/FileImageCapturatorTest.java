package com.d45labs.shapedetector.capture;

import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author osvaldo
 */
public class FileImageCapturatorTest extends ImageInspectorTest {

    /**
     * Test of getImage method, of class FileImageCapturator.
     */
    @Test
    public void testGetImage() {
        System.out.println("getImage");
        FileImageCapturator instance = new FileImageCapturator(FileImageCapturator.class.getClassLoader().getResource("files/paimei.jpg").getFile());
        BufferedImage expResult = null;
        try {
            expResult = ImageIO.read(FileImageCapturator.class.getClassLoader().getResource("files/paimei.jpg").openStream());
        } catch (IOException ex) {
            Logger.getLogger(FileImageCapturatorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        BufferedImage result = instance.getImage();
        assertFalse(result.equals(expResult));
    }

    @Test
    public void testNullImage() {
        System.out.println("getImage against null.");
        FileImageCapturator instance = new FileImageCapturator(null);
        BufferedImage expResult = null;
        BufferedImage result = instance.getImage();
        assertEquals(result, expResult);
    }

    @Test
    public void testIsContinue() {
        System.out.println("isContinue");
        FileImageCapturator instance = new FileImageCapturator(null);
        boolean expResult = false;
        boolean result = instance.isContinue();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetCaptureSize() {
        System.out.println("getCaptureSize");
        FileImageCapturator instance = new FileImageCapturator(null);
        int expResult = 1;
        int result = instance.getCaptureSize();
        assertEquals(expResult, result);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testNext() {
        System.out.println("next");
        FileImageCapturator instance = new FileImageCapturator(null);
        instance.next();
        fail("i cant reach this position.");
    }

    @Test
//    @Ignore
    public void testView() {
        FileImageCapturator instance = new FileImageCapturator(getClass().getResource("/files/paimei.jpg").getFile());
        instance.beforeCapture();
        final BufferedImage result = instance.getImage();
        instance.afterCapture();
        this.showImage(result);
    }
}
