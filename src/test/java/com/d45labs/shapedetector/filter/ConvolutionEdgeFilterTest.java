package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.utils.ImageOperations;
import com.d45labs.shapedetector.utils.ImageUtils;
import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author juancavallotti
 */
public class ConvolutionEdgeFilterTest extends ImageInspectorTest {

    BufferedImage image;

    @Before
    public void setUp() {

        FileImageCapturator fileImageCapturator = new FileImageCapturator(getClass().getResource("/files/figures.png").getFile());
        fileImageCapturator.beforeCapture();
        image = fileImageCapturator.getImage();
        fileImageCapturator.afterCapture();
    }

    /**
     * Test of process method, of class Rgb2GrayFilter.
     */
    @Test 
    @Ignore
    public void testProcess() {
        
        final float[][] operators = {
            {-1, 0, 0, 1}, //roberts x operator
            {0, -1, 1, 0}, //roberts y operator
        };
        
        ConvolutionEdgeFilter instance = new ConvolutionEdgeFilter(operators, 2);
        instance.beforeFilter();
        this.showImage(image, 5000);
        
        Rgb2GrayFilter filter = new Rgb2GrayFilter();
        filter.beforeFilter();
        image = filter.process(image)[0];
        
        BufferedImage[] process = instance.process(image);
        instance.afterFilter();
        for (final BufferedImage bufferedImage : process) {
            this.showImage(bufferedImage, 5000);
        }
//        assertEquals(expResult, result);

    }
    
    @Test
    public void testCannyFilter() throws Exception {
        CannyEdgeFilter cannyFilter = new CannyEdgeFilter(80, 20);
        cannyFilter.beforeFilter();
        
        Rgb2GrayFilter filter = new Rgb2GrayFilter();
        filter.beforeFilter();
        this.showImage(image);
        image = filter.process(image)[0];
        
        BufferedImage[] process = cannyFilter.process(image);
        cannyFilter.afterFilter();
        
        for (BufferedImage bufferedImage : process) {
            ImageOperations.fixSimpleDiscontinuities(bufferedImage);
            this.showImage(bufferedImage, 10000);
        }
    }
    
    @Test @Ignore
    public void testGenericConvolution() {
        
        double[][] kernel = {
            {-1,  0, 1},
            {-1 , 0, 1},
            {-1 , 0, 1},
        };
//        double[][] kernel = {
//            {-1,  -1, -1},
//            { 0,   0,  0},
//            { 1,   1,  1},
//        };
        
        Rgb2GrayFilter filter = new Rgb2GrayFilter();
        filter.beforeFilter();
        
        image = filter.process(image)[0];
        
        double[][] result = ImageOperations.convolveImage(image, kernel);
        
        this.showImage(ImageUtils.toGrayImage(result, 128), 10000);
        
        assertEquals(image.getWidth(), result.length);
        assertEquals(image.getHeight(), result[0].length);
    }
}
