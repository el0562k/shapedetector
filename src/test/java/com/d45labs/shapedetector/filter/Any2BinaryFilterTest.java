
package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author osvaldo
 */
public class Any2BinaryFilterTest extends ImageInspectorTest{
    
    public Any2BinaryFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }

    /**
     * Test of process method, of class Gray2BinaryFilter.
     */
    @Test
    public void testProcess() {
        System.out.println("process");
        FileImageCapturator fileImageCapturator = new FileImageCapturator(this.getClass().getClassLoader().getResource("files/paimei.jpg").getFile());
        BufferedImage image = fileImageCapturator.getImage();
        Rgb2GrayFilter rgb2gray = new Rgb2GrayFilter();
        Any2BinaryFilter instance = new Any2BinaryFilter(128);
        this.showImage(image,5000);
        rgb2gray.beforeFilter();
        BufferedImage[] process = rgb2gray.process(image);
        rgb2gray.afterFilter();
        for (BufferedImage bufferedImage : process) {
            this.showImage(bufferedImage,5000);
            instance.beforeFilter();
            BufferedImage[] process1 = instance.process(bufferedImage);
            instance.afterFilter();
            for (BufferedImage bufferedImage1 : process1) {
                this.showImage(bufferedImage1,5000);
            }
        }
//        BufferedImage[] result = instance.process(image);
//        for (BufferedImage bufferedImage : result) {
//            this.showImage(bufferedImage,5000);
//        }
    }
}
