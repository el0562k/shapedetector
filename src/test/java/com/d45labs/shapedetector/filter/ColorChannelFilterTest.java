/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author osvaldo
 */
public class ColorChannelFilterTest extends ImageInspectorTest{
    
    public ColorChannelFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }


    /**
     * Test of process method, of class ColorChannelFilter.
     */
    @Test
    public void testProcess() {
        System.out.println("process");
        BufferedImage image = getDefaultImage();
        ColorChannelFilter instance = new ColorChannelFilter();
        showImage(image, 2000);
        BufferedImage[] result = instance.process(image);
        for (BufferedImage bufferedImage : result) {
            this.showImage(bufferedImage,2000);
        }
        
    }
}
