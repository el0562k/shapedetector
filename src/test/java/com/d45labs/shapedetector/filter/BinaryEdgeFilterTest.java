package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author osvaldo
 */
public class BinaryEdgeFilterTest extends ImageInspectorTest {

    public BinaryEdgeFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of process method, of class BinaryEdgeFilter.
     */
    @Test
//    @Ignore
    public void testProcess() {
        System.out.println("process");
        Any2BinaryFilter filter = new Any2BinaryFilter(128);
        BinaryEdgeFilter instance = new BinaryEdgeFilter(1,3);
        instance.beforeFilter();
        filter.beforeFilter();
        BufferedImage defaultImage = this.getDefaultImage();
        this.showImage(defaultImage);
        BufferedImage[] process = filter.process(defaultImage);
        for (BufferedImage bufferedImage : process) {
            this.showImage(bufferedImage);
            BufferedImage[] process1 = instance.process(bufferedImage);
            for (BufferedImage bufferedImage1 : process1) {
                this.showImage(bufferedImage1, 2000);
            }
        }

        filter.afterFilter();
        instance.afterFilter();
        try {
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BinaryEdgeFilterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    @Ignore
    public void testProcessMultiFilter() {
        System.out.println("process");
        FileImageCapturator fileImageCapturator = new FileImageCapturator(this.getClass().getClassLoader().getResource("files/figures.png").getFile());
        ColorChannelFilter ccf = new ColorChannelFilter();
        Any2BinaryFilter binary = new Any2BinaryFilter(128);
        BinaryEdgeFilter edge = new BinaryEdgeFilter(1,3);

        edge.beforeFilter();
        ccf.beforeFilter();
        binary.beforeFilter();
        BufferedImage defaultImage = fileImageCapturator.getImage();
        this.showImage(defaultImage);
        BufferedImage[] colorFilterImages = ccf.process(defaultImage);
        for (BufferedImage colorImage : colorFilterImages) {
            this.showImage(colorImage);
            BufferedImage[] binaryImages = binary.process(colorImage);
            for (BufferedImage binaryImage : binaryImages) {
                this.showImage(binaryImage);
                BufferedImage[] process = edge.process(binaryImage);
                for (BufferedImage bufferedImage : process) {
                    this.showImage(bufferedImage);
                }
            }
        }

        ccf.afterFilter();
        edge.afterFilter();
        binary.afterFilter();
        try {
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            Logger.getLogger(BinaryEdgeFilterTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
