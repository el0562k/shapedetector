/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.watch.ImageInspectorTest;
import java.awt.image.BufferedImage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author osvaldo
 */
public class Rgb2GrayFilterTest extends ImageInspectorTest {

    public Rgb2GrayFilterTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {

    }

    /**
     * Test of process method, of class Rgb2GrayFilter.
     */
    @Test
    public void testProcess() {
        System.out.println("process");
        BufferedImage image = this.getDefaultImage();
        Rgb2GrayFilter instance = new Rgb2GrayFilter();
        instance.beforeFilter();
        this.showImage(image,5000);
        BufferedImage[] process = instance.process(image);
        instance.afterFilter();
        for (final BufferedImage bufferedImage : process) {
            this.showImage(bufferedImage,5000);
        }        
//        assertEquals(expResult, result);

    }
}
