package com.d45labs.shapedetector.fourier;

import org.junit.Ignore;
import java.math.BigDecimal;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author osvaldo
 */
public class ComplexTest {
    
    public ComplexTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }
    
    @Before
    public void setUp() {
    }
    
    /**
     * Test of toString method, of class Complex.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        Complex instance = new Complex(1,1);
        String expResult = "1+1i";
        String result = instance.toString();
        System.out.println("result:"+result);
        assertEquals(expResult, result);
    }

    /**
     * Test of sum method, of class Complex.
     */
    @Test    
    public void testSum() {
        System.out.println("sum");
        Complex complex = new Complex(1, 2);
        Complex instance = new Complex(1,2);
        instance.sum(complex);
        
        System.out.println("result:"+instance);
        assertEquals(instance, new Complex(2, 4));
        
        
    }

    /**
     * Test of substract method, of class Complex.
     */
    @Test    
    public void testSubstract() {
        System.out.println("substract");
        Complex complex = new Complex(1,2);
        Complex instance = new Complex(1,2);
//        instance.substract(complex);
        
        System.out.println("result:"+instance);
        assertEquals(instance, new Complex(0, 0));
    }

    /**
     * Test of scalar method, of class Complex.
     */
    @Test
    public void testScalar() {
        System.out.println("scalar");
        BigDecimal scalar = BigDecimal.valueOf(2);
        Complex instance = new Complex(1,1);
        instance.scalar(scalar);
        System.out.println("result"+instance);
        assertEquals(instance, new Complex(2, 2));
    }

    /**
     * Test of multiply method, of class Complex.
     */
    @Test
    public void testMultiply() {
        System.out.println("multiply");
        Complex complex = new Complex(2,2);
        Complex instance = new Complex(2,2);
        instance.multiply(complex);
        System.out.println("result"+instance);
        assertEquals(instance, new Complex(0, 8));
    }

    /**
     * Test of equals method, of class Complex.
     */
    @Test    
    public void testEquals() {
        System.out.println("equals");
        Complex obj = new Complex(1,1);
        Complex instance = new Complex(1,1);
        boolean expResult = true;
        boolean result = instance.equals(obj);
        assertEquals(expResult, result);
        
    }

    /**
     * Test of mod method, of class Complex.
     */
    @Test
    public void testMod() {
        System.out.println("mod");
        Complex instance = new Complex(1,2);
        
        BigDecimal result = instance.mod();
        System.out.println("result"+result);
    }

    /**
     * Test of arg method, of class Complex.
     */
    @Test
    public void testArg() {
        System.out.println("arg");
        Complex instance = new Complex(0,1);
        BigDecimal expResult = BigDecimal.valueOf(1);
        BigDecimal result = instance.arg();
//        System.out.println("result:"+result);
//        assertEquals(expResult, result);    
        
    }

    /**
     * Test of euler method, of class Complex.
     */
    @Test
    public void testEuler() {
        System.out.println("euler");
//        Complex instance = new Complex(1,1);
//        Complex instance = new Complex(0,Math.PI);
        Complex expResult = new Complex(-1,0);
//        Complex expResult = new Complex(Math.cos(1), Math.sin(0.84147));
        Complex result = Complex.euler(Math.PI);
        System.out.println("result:"+result);
//        assertEquals(expResult, result);
    }
}
