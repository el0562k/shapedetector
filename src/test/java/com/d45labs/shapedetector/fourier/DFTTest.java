/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.d45labs.shapedetector.fourier;

import org.junit.Ignore;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author osvaldo
 */
public class DFTTest {

    public DFTTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

//    /**
//     * Test of transform method, of class DFT.
//     */
    @Test
    @Ignore
    public void testTransform() {
        System.out.println("transform complex without loss");
        Complex[] samples = this.sampleGenerator(new double[]{0, 0, 0, 1, 0, 0, 0});
//        Complex[] samples = this.sampleGenerator(new double[]{0, 1, 0 });
        DFT instance = new DFT();
        
        System.out.print("samples:");
        for (Complex complex : samples) {
            System.out.print(","+complex);
        }
        System.out.println("");
//        Complex[] expResult = null;
        Complex[] result = instance.transform(samples);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
        System.out.print("transform:");
        for (Complex complex : result) {
            System.out.print(","+complex);
        }
    }
    /**
     * Test of transform method, of class DFT.
     */

    @Test
//    @Ignore
    public void testTransformNative() {
        System.out.println("transform with native complex");
        NativeComplex[] samples = this.sampleNativeGenerator(new double[]{0, 0, 0, 1, 0, 0, 0});
//        NativeComplex[] samples = this.sampleNativeGenerator(new double[]{1, 1, 1, 2, 1, 1, 1});
//        NativeComplex[] samples = this.sampleNativeGenerator(new double[]{0, 1, 0 });
        DFT instance = new DFT();
        
        System.out.print("samples:");
        for (NativeComplex complex : samples) {
            System.out.print(","+complex);
        }
        System.out.println("");
//        Complex[] expResult = null;
        NativeComplex[] result = instance.transform(samples);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
        System.out.println("transform:");
        for (NativeComplex complex : result) {
            System.out.print(","+complex);
        }
        System.out.println("");
        NativeComplex[] revert = instance.revert(result);
        System.out.println("revert:");
        for (NativeComplex complex : revert) {
            System.out.print(","+complex);
        }
        
    }

    private Complex[] sampleGenerator(double[] samples) {
        Complex[] complexes = new Complex[samples.length];
        for (int i = 0; i < complexes.length; i++) {
            complexes[i] = new Complex(samples[i],0);
        }
        return complexes;
    }
    private NativeComplex[] sampleNativeGenerator(double[] samples) {
        NativeComplex[] complexes = new NativeComplex[samples.length];
        for (int i = 0; i < complexes.length; i++) {
            complexes[i] = new NativeComplex(samples[i],0);
        }
        return complexes;
    }
//    /**
//     * Test of computePhi method, of class DFT.
//     */
//    @Test
//    public void testComputePhi() {
//        System.out.println("computePhi");
//        Complex[] values = null;
//        DFT instance = new DFT();
//        Double[] expResult = null;
//        Double[] result = instance.computePhi(values);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of computeArgument method, of class DFT.
//     */
//    @Test
//    public void testComputeArgument() {
//        System.out.println("computeArgument");
//        Complex[] values = null;
//        DFT instance = new DFT();
//        Double[] expResult = null;
//        Double[] result = instance.computeArgument(values);
//        assertEquals(expResult, result);
//        // TODO review the generated test code and remove the default call to fail.
//        fail("The test case is a prototype.");
//    }
}
