package com.d45labs.shapedetector.watch;

import java.awt.Graphics;
import java.awt.image.BufferedImage;
import javax.swing.JPanel;

/**
 *
 * @author osvaldo
 */
public abstract class ImagePanel extends JPanel {

    
    
    @Override
    protected void paintComponent(Graphics g) {
        g.drawImage(getImage(), 0, 0, null);
    }
    
    public abstract BufferedImage getImage();
}
