package com.d45labs.shapedetector.watch;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.model.capture.ImageCapturator;
import java.awt.image.BufferedImage;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author osvaldo
 */
public class ImageInspectorTest {
    /**
     * shows an image about 10 seconds.
     * @param image 
     */
    protected void showImage(final BufferedImage image) {
        try {
            java.awt.EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    new ImageInspector(image).setVisible(true);
                }
            });
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ImageInspectorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected void showImage(final BufferedImage image,long sleepTime) {
        try {
            java.awt.EventQueue.invokeLater(new Runnable() {

                @Override
                public void run() {
                    new ImageInspector(image).setVisible(true);
                }
            });
            Thread.sleep(sleepTime);
        } catch (InterruptedException ex) {
            Logger.getLogger(ImageInspectorTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    public ImageCapturator getDefaultCapturator(){
        return new FileImageCapturator(this.getClass().getClassLoader().getResource("files/paimei.jpg").getFile());
//        return new FileImageCapturator(this.getClass().getClassLoader().getResource("files/figures.png").getFile());
    }
    
    public BufferedImage getDefaultImage(){
        ImageCapturator defaultCapturator = getDefaultCapturator();
        defaultCapturator.beforeCapture();
        BufferedImage image = defaultCapturator.getImage();
        defaultCapturator.afterCapture();
        return image;
    }
    

    
}
