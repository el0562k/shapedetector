package com.d45labs.shapedetector.feature;

import com.d45labs.shapedetector.capture.FileImageCapturator;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.utils.Point;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author juancavallotti
 */
public class ObjectPointsGetterTest {

    BufferedImage image;

    @Before
    public void setUp() {

        FileImageCapturator fileImageCapturator = new FileImageCapturator(getClass().getResource("/files/figures.png").getFile());
        fileImageCapturator.beforeCapture();
        image = fileImageCapturator.getImage();
        fileImageCapturator.afterCapture();
    }
    
    @Test
    public void testObjectPointsGetter() {
        
        ObjectPointsGetter getter = new ObjectPointsGetter();
        getter.before();
        FeatureAdapter<Set<Point>> adapter = getter.computeFeatures(image);
        getter.after();
        
        adapter.before();
        List list = adapter.adapt();
        adapter.after();
        
        assertTrue(list.size() >= 6);
        
        
    }
    
}
