package com.d45labs.shapedetector.morphologies;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author osvaldo
 */
public class MorphologyOperationsTest {

    public MorphologyOperationsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    /**
     * Test of image2array method, of class MorphologyOperations.
     */
    @Test
    @Ignore
    public void testImage2array() {
        System.out.println("image2array");
    }

    /**
     * Test of array2Image method, of class MorphologyOperations.
     */
    @Test
    @Ignore
    public void testArray2Image() {
        System.out.println("array2Image");
    }

    /**
     * Test reflection
     */
    @Test
    public void testReflects() {
        System.out.println("reflects");
        boolean[][] source = {{true, false, false}, {false, true, false}, {false, false, false}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{false, false, false}, {false, true, false}, {false, false, true}};
        boolean[][] result = instance.reflects(source);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of complement method, of class MorphologyOperations.
     */
    @Test
    public void testComplement() {
        System.out.println("complement");
        boolean[][] source = {{true, false, false}, {false, true, false}, {false, false, false}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] result = instance.complement(source);
        boolean[][] expResult = {{false, true, true}, {true, false, true}, {true, true, true}};
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of substract method, of class MorphologyOperations.
     */
    @Test
    public void testSubstract() {
        System.out.println("substract");
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] A = instance.mergeSubspaceAt(0, 0, instance.ones(5), instance.zeros(10));
        this.printToSout(A, "A");
        boolean[][] B = instance.mergeSubspaceAt(4, 4, instance.ones(6), instance.zeros(10));
        this.printToSout(B, "B");
        boolean[][] expResult = instance.mergeSubspaceAt(0, 0, instance.ones(5), instance.zeros(10));
        expResult[4][4]=false;
        this.printToSout(expResult, "expected result");
        boolean[][] result = instance.substract(A, B);
        this.printToSout(result, "result");
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of dilate method, of class MorphologyOperations.
     */
    @Test
    public void testDilate() {
        System.out.println("dilate");
        boolean[][] se = {{true, true}};
        boolean[][] A = {{false, true, false, false}, {false, true, false, false}, {false, true, true, false}, {true, false, false, false}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{false, true, true, false}, {false, true, true, false}, {false, true, true, true}, {true, true, false, false}};
        boolean[][] result = instance.dilate(se, A);
        assertArrayEquals(expResult, result);
    }  

    /**
     * Test of subspace 
     */
    @Test
    public void testSubspace() {
        System.out.println("subspace");
        int x = 1;
        int y = 1;
        int deltax = 2;
        int deltay = 2;
        boolean[][] A = {{true, true, true, true}, {true, false, false, true}, {true, false, false, true}, {true, true, true, true}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{false, false}, {false, false}};
        boolean[][] result = instance.subspace(x, y, deltax, deltay, A);
        assertArrayEquals(expResult, result);
    }

    /**
     * Test of mergeSubspaceAt 
     */
    @Test
//    @Ignore
    public void testMergeSubspaceAt() {
        System.out.println("mergeSubspaceAt");
        int x = 0;
        int y = 0;
        boolean[][] subspace = {{false, false}, {false, false}};
        boolean[][] A = {{true, true, true, true}, {true, false, false, true}, {true, false, false, true}, {true, true, true, true}};
        MorphologyOperations instance = new MorphologyOperations();
        instance.mergeSubspaceAt(x, y, subspace, A);
        boolean[][] expResult = {{false, false, true, true}, {false, false, false, true}, {true, false, false, true}, {true, true, true, true}};
        assertArrayEquals(expResult, A);
    }

    /**
     * Test of translate method, of class MorphologyOperations.
     */
    @Test
    public void testTranslate() {
        System.out.println("translate");
        int x = 2;
        int y = 2;
        boolean[][] A = {{true, true, true, true, true}, {true, false, false, true, true}, {true, false, false, true, true}, {true, true, true, true, true}, {true, true, true, true, true}};
        boolean[][] se = {{true}, {true}, {true}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{false, false, false, false, false}, {false, false, true, false, false}, {false, false, true, false, false}, {false, false, true, false, false}, {false, false, false, false, false}};
        boolean[][] result = instance.translate(x, y, A, se);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testTranslateLowerBoundary() {
        System.out.println("translate-lower-boundary");
        int x = 0;
        int y = 0;
        boolean[][] A = {{true, true, true, true, true}, {true, false, false, true, true}, {true, false, false, true, true}, {true, true, true, true, true}, {true, true, true, true, true}};
        boolean[][] se = {{true}, {true}, {true}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{true, false, false, false, false}, {true, false, false, false, false}, {false, false, false, false, false}, {false, false, false, false, false}, {false, false, false, false, false}};
        boolean[][] result = instance.translate(x, y, A, se);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testTranslateUpperBoundary() {
        System.out.println("translate-upper-boundary");
        int x = 4;
        int y = 4;
        boolean[][] A = {{true, true, true, true, true}, {true, false, false, true, true}, {true, false, false, true, true}, {true, true, true, true, true}, {true, true, true, true, true}};
        boolean[][] se = {{true}, {true}, {true}};
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] expResult = {{false, false, false, false, false}, {false, false, false, false, false}, {false, false, false, false, false}, {false, false, false, false, true}, {false, false, false, false, true}};
        boolean[][] result = instance.translate(x, y, A, se);
        assertArrayEquals(expResult, result);
    }

    @Test
    public void testEdge() {
        System.out.println("test edge");
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] A = {{true, true, true, false, true, true, true, true, true, false}, {true, true, true, false, true, true, true, true, true, false}, {true, true, true, true, true, true, true, true, true, true}, {true, true, true, true, true, true, true, true, true, true}, {true, true, true, true, true, true, true, true, true, true}};
        this.printToSout(A, "A");
        boolean[][] se = {{true, true, true}, {true, true, true}, {true, true, true}};
        this.printToSout(se, "SE");
        boolean[][] erode = instance.erode(se, A);
        this.printToSout(erode, "Erosion");
        boolean[][] result = instance.substract(A, erode);
        this.printToSout(result, "Result");
        boolean[][] expResult = {{true, true, true, false, true, true, true, true, true, false}, {true, false, true, false, true, false, false, false, true, false}, {true, false, true, true, true, false, false, false, true, true}, {true, false, false, false, false, false, false, false, false, true}, {true, true, true, true, true, true, true, true, true, true}};
        this.printToSout(expResult, "expected result");
        assertArrayEquals(expResult, result);
    }

    private void printToSout(boolean[][] A, String title) {
        System.out.println("Writing:" + title);
        for (int i = 0; i < A.length; i++) {
            for (int j = 0; j < A[0].length; j++) {
                System.out.print(A[i][j] ? "1" : "0");
            }
            System.out.println("");
        }
        System.out.println("");
    }

    @Test
//    @Ignore
    public void erodeTest2() {
        System.out.println("erode boundary test");
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] se = {{true},{true},{true}};
//        this.printToSout(se, "se");
        boolean[][] A = instance.mergeSubspaceAt(1, 1, instance.ones(3), instance.zeros(5));
//        this.printToSout(A, "A");
        boolean[][] expResult = instance.mergeSubspaceAt(2, 1,new boolean[][] {{true,true,true}}, instance.zeros(5));
        boolean[][] result = instance.erode(se, A);
//        this.printToSout(result, "result");
//        this.printToSout(expResult, "expeted result");
        assertArrayEquals(expResult, result);
    }
    
    @Test
    @Ignore
    public void erodeTest3(){
        System.out.println("erode boundary test2");
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] se = instance.ones(3);
        boolean[][] A = instance.ones(5);
//        this.printToSout(se, "se");
//        this.printToSout(A, "A");
        boolean[][] expResult = instance.mergeSubspaceAt(1, 1, instance.ones(3), instance.zeros(5));
        boolean[][] result = instance.erode(se, A);
//        this.printToSout(result, "result");
//        this.printToSout(expResult, "expected result");
        assertArrayEquals(expResult, result);
    }
    
    @Test
    public void borderize(){
        System.out.println("borderize");
        MorphologyOperations instance = new MorphologyOperations();
        boolean[][] se = instance.ones(4);
//        this.printToSout(se, "se");
        boolean[][] A = instance.ones(10);
//        this.printToSout(A, "A");
        boolean[][] result = instance.borderize(se,A);
//        this.printToSout(result, "result");
        boolean[][] expResult = instance.mergeSubspaceAt(2, 2, instance.ones(10), instance.zeros(14));
//        this.printToSout(expResult, "expected result");
        assertArrayEquals(expResult, result);        
    }
  
    
}
