package com.d45labs.shapedetector.config;

import javax.swing.JPanel;

/**
 * a kind of hook for each component that wants to be configured on runtime,
 * although provides a factory method to create a component itself.
 * @author osvaldo
 */
public interface Configurable<T>{
    /**
     * gets the current configuration of this factory.
     * @return 
     */
    JPanel getConfigurator();
    /**
     * factory method to create detector components
     * @return 
     */
    T createComponent();
    
}
