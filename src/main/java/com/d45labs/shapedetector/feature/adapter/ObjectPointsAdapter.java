package com.d45labs.shapedetector.feature.adapter;

import com.d45labs.shapedetector.feature.impl.ObjectPointsFeautre;
import com.d45labs.shapedetector.model.feature.Feature;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.utils.ImageUtils;
import com.d45labs.shapedetector.utils.Point;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author juancavallotti
 */
public class ObjectPointsAdapter implements FeatureAdapter<Set<Point>> {
    
    private final BufferedImage image;
    
    private Set<Point> visitedPoints;
    
    public ObjectPointsAdapter(BufferedImage image) {
        this.image = image;
    }
    
    @Override
    public void before() {
        visitedPoints = new HashSet<Point>();
    }

    @Override
    public void after() {
        visitedPoints.clear();
    }

    @Override
    public List<Feature<Set<Point>>> adapt() {
        
        List<Feature<Set<Point>>> ret = new LinkedList<Feature<Set<Point>>>();
        
        for (int i = 0; i < image.getWidth(); i++) {
            for (int j = 0; j < image.getHeight(); j++) {
                
                int level = ImageUtils.safeReadGrayPixel(i, j, image);
                
                if (level == 0) {
                    continue;
                }
                
                Point p = new Point(i, j);
                
                if (visitedPoints.contains(p)) {
                    continue; //the point has already been visited
                }
                
                Set<Point> points = getPointsInShape(p, image);
                ret.add(new ObjectPointsFeautre(points));
            }
        }
        
        return ret;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName();
    }

    private Set<Point> getPointsInShape(Point p, BufferedImage image) {
        
        Set<Point> ret = new HashSet<Point>();
        addNeighborPoints(p, image, ret);
        return ret;
    }
    
    
    private void addNeighborPoints(Point p, BufferedImage image, Set<Point> points) {
        
        if (points.contains(p)) {
            return;
        } else {
            points.add(p);
        }
        
        visitedPoints.add(p);
        
        List<Point> neighborhs = expandNeighbors(p, image);
        
        for (Point point : neighborhs) {
            addNeighborPoints(point, image, points);
        }
        
    }
    private List<Point> expandNeighbors(Point point, BufferedImage image) {

        List<Point> ret = new LinkedList<Point>();

        //read the 8 neighbors
        putImagePoint(point.getX() - 1, point.getY() - 1, image, ret); //upper left
        putImagePoint(point.getX(), point.getY() - 1, image, ret); //upper
        putImagePoint(point.getX() + 1, point.getY() - 1, image, ret); //upper right
        putImagePoint(point.getX() + 1, point.getY(), image, ret); //right
        putImagePoint(point.getX() + 1, point.getY() + 1, image, ret); //lower right
        putImagePoint(point.getX(), point.getY() + 1, image, ret); //lower
        putImagePoint(point.getX() - 1, point.getY() + 1, image, ret); //lower left;
        putImagePoint(point.getX() - 1, point.getY(), image, ret); //left
        
        return ret;

    }
    
    private void putImagePoint(int x, int y, BufferedImage image, List<Point> ret) {
        int color = ImageUtils.safeReadGrayPixel(x, y, image);
        
        if (color == 0) {
            //we just dont disturb because it is a useless point.
            return;
        }
        
        ret.add(new Point(x, y));
    }
}
