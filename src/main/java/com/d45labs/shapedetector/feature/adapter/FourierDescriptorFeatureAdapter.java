package com.d45labs.shapedetector.feature.adapter;

import com.d45labs.shapedetector.model.feature.Feature;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import java.util.List;

/**
 *
 * @author osvaldo
 */
public class FourierDescriptorFeatureAdapter implements FeatureAdapter {

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Feature> adapt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getName() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public void setSample(int[] histogram) {
        throw new UnsupportedOperationException("Not yet implemented");
    }
    
}
