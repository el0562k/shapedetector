package com.d45labs.shapedetector.feature.impl;

import com.d45labs.shapedetector.model.feature.Feature;
import com.d45labs.shapedetector.utils.Point;
import java.io.Serializable;
import java.util.Set;

/**
 * Represents all the points of a single object.
 * @author juancavallotti
 */
public class ObjectPointsFeautre implements Feature<Set<Point>>, Serializable {
    private static final long serialVersionUID = 1L;
    
    private Set<Point> points;

    public ObjectPointsFeautre() {
    }
    
    public ObjectPointsFeautre(Set<Point> points) {
        this.points = points;
    }
    
    
    @Override
    public Set<Point> get() {
        return points;
    }

    @Override
    public void set(Set<Point> t) {
        points = t;
    }

    @Override
    public boolean equals(Feature<Set<Point>> t) {
        
        if (this.points == null && t.get() == null) {
            return true;
        }
        
        if (this.points == null) {
            return false;
        }
        
        return this.points.equals(t.get());
    }
    
    
    
}
