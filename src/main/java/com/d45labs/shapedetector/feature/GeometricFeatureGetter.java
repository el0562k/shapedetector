package com.d45labs.shapedetector.feature;

import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.model.feature.FeatureGetter;
import java.awt.image.BufferedImage;

/**
 *
 * @author osvaldo
 */
public class GeometricFeatureGetter implements FeatureGetter {

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public FeatureAdapter computeFeatures(BufferedImage image) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
