package com.d45labs.shapedetector.feature;

import com.d45labs.shapedetector.feature.adapter.FourierDescriptorFeatureAdapter;
import com.d45labs.shapedetector.fourier.DFT;
import com.d45labs.shapedetector.fourier.NativeComplex;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.model.feature.FeatureGetter;
import com.d45labs.shapedetector.morphologies.MorphologyOperations;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

/**
 * uses fourier to describe a closed shape. features should be values from an discretized constant fourier analysis.
 * 
 * process is defined as:
 * -an get images, (image conditions should be: binary image and de-noised) 
 * -dicretize them to ensure to gets a one-line-dimension
 * -find shapes
 * -use dft to transform this shape to from d2 problem to 1d.
 * -extract argument 
 * -discretize argument spectre (from 0 to 2*pi)
 * -generate feature vector.
 * 
 * @author osvaldo
 */
public class FourierDescriptorFeatureGetter implements FeatureGetter {

    private MorphologyOperations mo = new MorphologyOperations();
    private int baseSample;
    private int deltaHist;
    private int neigborSize;
    private int minNeigborSize;
    private int rejectSampleSize;
    private DFT dft = new DFT();

    @Override
    public void before() {
    }

    @Override
    public void after() {
    }

    @Override
    public FeatureAdapter computeFeatures(BufferedImage image) {
        //get an image. and binarize it.
        boolean[][] image2array = mo.image2array(image);

//        boolean[][] normalizedImage = this.normalize(image2array, baseSample);
        FourierDescriptorFeatureAdapter dfda = new FourierDescriptorFeatureAdapter();

        NativeComplex[] shape;

        do {
            shape = getShapeAndRemoveFrom(image2array, neigborSize, rejectSampleSize);
            if (shape != null) {
                NativeComplex[] transform = transform(shape);
                int[] histogram = computeHistogram(transform, deltaHist);
                dfda.setSample(histogram);
            }
        } while (shape != null);


        return dfda;
    }

//    private boolean[][] normalize(boolean[][] base, int baseSample) {
//
//        return null;
//    }
    /**
     * finds a shape, remove it and drawit as
     * as additional, this process remove noises.
     * @param normalizedImage
     * @return 
     */
    private NativeComplex[] getShapeAndRemoveFrom(boolean[][] normalizedImage, int neigborSize, int rejectSampleSize) {
        List<NativeComplex> list = new ArrayList<NativeComplex>();
        boolean doBreak = true;

        for (int i = 0; i < normalizedImage.length; i++) {
            for (int j = 0; j < normalizedImage[0].length; j++) {
                //simple scan.                
                if (normalizedImage[i][j] == true) {
                    list.add(new NativeComplex(i, j));

                    int firstX = i;
                    int firstY = j;
                    //scan neigbors
//                    do {
//                        
//
//                    } while (firstX ==  && firstY == );


                    if (list.size() <= rejectSampleSize) {
                        //remove noise.
                        for (NativeComplex nativeComplex : list) {
                            normalizedImage[(int) nativeComplex.getReal()][(int) nativeComplex.getImg()] = false;
                        }
                        list.clear();
                    }
                }
                if (doBreak) {
                    break;
                }
            }
            if (doBreak) {
                break;
            }
        }

        return list.toArray(new NativeComplex[list.size()]);
    }

    private NativeComplex[] transform(NativeComplex[] shape) {
        return dft.transform(shape);
    }

    private int[] computeHistogram(NativeComplex[] transform, int deltaHist) {
        double[] samples = new double[transform.length];

        for (int i = 0; i < samples.length; i++) {
            samples[i] = transform[i].arg();
        }
        double delta = (2d * Math.PI) / deltaHist;
        int[] histogram = new int[deltaHist];
        for (int i = 0; i < samples.length; i++) {
            int value = (int) (samples[i] % delta);
            histogram[value]++;
        }
        return histogram;
    }
}
