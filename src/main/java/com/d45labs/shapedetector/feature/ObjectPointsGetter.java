package com.d45labs.shapedetector.feature;

import com.d45labs.shapedetector.feature.adapter.ObjectPointsAdapter;
import com.d45labs.shapedetector.filter.CannyEdgeFilter;
import com.d45labs.shapedetector.filter.Rgb2GrayFilter;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.model.feature.FeatureGetter;
import com.d45labs.shapedetector.utils.ImageOperations;
import com.d45labs.shapedetector.utils.Point;
import java.awt.image.BufferedImage;
import java.util.Set;

/**
 * Feature getter that discovers separate objects on an image and returns lists
 * of the objects points.
 * @author juancavallotti
 */
public class ObjectPointsGetter implements FeatureGetter<Set<Point>> {

    @Override
    public void before() {
        
    }

    @Override
    public void after() {
        
    }

    /**
     * Converts the image to grayscale and discovers the edges by using canny
     * edge detector.
     * @param image
     * @return 
     */
    @Override
    public FeatureAdapter<Set<Point>> computeFeatures(BufferedImage image) {
        
        //convert the image to grayscale.
        //rgb2gray filter cycle
        Rgb2GrayFilter filter = new Rgb2GrayFilter();
        filter.beforeFilter();
        image = filter.process(image)[0];
        filter.afterFilter();;
        
        //find the edges.
        CannyEdgeFilter cannyFilter = new CannyEdgeFilter(80, 20);
        cannyFilter.beforeFilter();
        BufferedImage[] process = cannyFilter.process(image);
        cannyFilter.afterFilter();
        
        image = process[0];
        
        ImageOperations.fixSimpleDiscontinuities(image);
        
        return new ObjectPointsAdapter(image);
    }

}
