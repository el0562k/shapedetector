package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import com.d45labs.shapedetector.utils.ImageOperations;
import com.d45labs.shapedetector.utils.ImagePoint;
import com.d45labs.shapedetector.utils.ImageUtils;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author juancavallotti
 */
public class CannyEdgeFilter implements Filter {

    private static final int black = 0x00; //0
    private static final int white = 0xFF; //255
    private static final double K45DEG = Math.PI / 4;
    
    
    private final double[][] kernelX = {
        {-1, 0, 1},
        {-2, 0, 2},
        {-1, 0, 1},};
    private final double[][] kernelY = {
        {-1, -2, -1},
        {0, 0, 0},
        {1, 2, 1},};

//    private final double[][] kernelX = {
//        {-1, 0},
//        { 0, 1},
//    };
//    private final double[][] kernelY = {
//        { 0, -1},
//        { 1,  0},
//    };
    //cache to prevent inspecting already visited pixels.
    private Set<ImagePoint> visitedPoints;
    private int highThreshold;
    private int lowThreshold;

    public CannyEdgeFilter(int highThreshold, int lowThreshold) {
        this.highThreshold = highThreshold;
        this.lowThreshold = lowThreshold;
    }

    /**
     * Standard constructor with default values.
     */
    public CannyEdgeFilter() {
        this(100, 20);
    }

    @Override
    public void beforeFilter() {
        visitedPoints = new HashSet<ImagePoint>();
    }

    @Override
    public void afterFilter() {
        visitedPoints.clear();
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {

        double[][] xEdge = ImageOperations.convolveImage(image, kernelX);
        double[][] yEdge = ImageOperations.convolveImage(image, kernelY);

        BufferedImage ret = applyCannyFiltering(xEdge, yEdge);

        return new BufferedImage[]{ret};
    }

    private BufferedImage applyCannyFiltering(double[][] xEdge, double[][] yEdge) {
        
        double[][] ret = new double[xEdge.length][xEdge[0].length];
        
        nonMaximaSupress(xEdge, yEdge, ret);
        hysteresisThresholding(ret);

        return ImageUtils.toGrayImage(ret);
    }

    private void nonMaximaSupress(double[][] xEdge, double[][] yEdge, double[][] ret) {
        //maximize the gradient
        for (int i = 0; i < ret.length; i++) {
            for (int j = 0; j < ret[0].length; j++) {
                ret[i][j] = getAppropriatePixel(i, j, xEdge, yEdge);
            } //end j for
        } //end i for
    }

    /**
     * Get the pixel value if it is a local maxima, otherwise return 0.
     *
     * @param x
     * @param y
     * @param xEdge
     * @param yEdge
     * @return
     */
    private double getAppropriatePixel(int x, int y, double[][] xEdge, double[][] yEdge) {

        double module = pointMod(x, y, xEdge, yEdge);
        double direction = pointDirection(x, y, xEdge, yEdge);

        double beforeMod;
        double afterMod;

        //System.out.println(direction);
        EdgeDirection dir = standarizeDirection(direction);
        switch (dir) {
            case UNDEFINED:
                return black;
            case HORIZONTAL:
                //the direction of the edge is horizontal
                beforeMod = pointMod(x - 1, y, xEdge, yEdge);
                afterMod = pointMod(x + 1, y, xEdge, yEdge);
                break;
            case VERTICAL:
                //the direction of the edge is vertical
                beforeMod = pointMod(x, y - 1, xEdge, yEdge);
                afterMod = pointMod(x, y + 1, xEdge, yEdge);
                break;
            default:
                beforeMod = -1;
                afterMod = -1;
        }

        if (beforeMod == -1 || afterMod == -1) {
            return black;
        }

        boolean ret = module > beforeMod && module > afterMod;

        if (ret) {
            return module;
        }

        return black;
    }

    private double pointMod(int x, int y, double[][] xEdge, double[][] yEdge) {

        double ret = -1;

        if (x < 0 || x >= xEdge.length) {
            return ret;
        }

        if (y < 0 || y >= yEdge[0].length) {
            return ret;
        }
        double horizontalValue = xEdge[x][y];
        double verticalValue = yEdge[x][y];
        ret = mod(horizontalValue, verticalValue);
        return ret;
    }

    double minLevel = 300;
    double maxLevel = 0;
    
    private double mod(double x, double y) {
        double module = Math.sqrt(x * x + y * y);
        
        if (module < minLevel) {
            minLevel = module; 
        }
        if (module > maxLevel) {
            maxLevel = module;
        }
        
        return module;
    }

    private double pointDirection(int x, int y, double[][] xEdge, double[][] yEdge) {

        double ret = -1;

        if (x < 0 || x >= xEdge.length) {
            return ret;
        }

        if (y < 0 || y >= yEdge[0].length) {
            return ret;
        }
        double horizontalValue = xEdge[x][y];
        double verticalValue = yEdge[x][y];
        ret = direction(horizontalValue, verticalValue);
        return ret;
    }

    double minDirection = 10;
    double maxDirection = -10;
    
    private double direction(double horizontalValue, double verticalValue) {
        double direction = Math.atan(verticalValue / horizontalValue);
        
        if (direction < minDirection) {
            minDirection = direction;
        }
        
        if (direction > maxDirection) {
            maxDirection = direction;
        }
        
        return direction;
    }

    EdgeDirection standarizeDirection(double angle) {

        
        if (Double.isNaN(angle)) {
            return EdgeDirection.UNDEFINED;
        }
        
        double absAngle = Math.abs(angle);
        
        if (absAngle <= K45DEG) {
            return EdgeDirection.HORIZONTAL;
        }
        
        if (absAngle > K45DEG) {
            return EdgeDirection.VERTICAL;
        }
        
        //if no other direction possible, then it is the main diagonal
        return EdgeDirection.UNDEFINED;
    }



    private void hysteresisThresholding(double[][] image) {
        //maximize the gradient
        for (int i = 0; i < image.length; i++) {
            for (int j = 0; j < image[0].length; j++) {
                int value = (int) Math.round(image[i][j]);
                if (value >= highThreshold) {
                    ImagePoint pixel = new ImagePoint(i, j, value);
                    enhanceEdgeNeighbors(pixel, image);
                } else {
                    image[i][j] = black; //black
                }
            } //end j for
        } //end i for        
    }

    /**
     * Recursive function make weak edges appear.
     *
     * @param pixel the starting point
     * @param image the pixels.
     */
    private void enhanceEdgeNeighbors(ImagePoint pixel, double[][] image) {
        
        if (visitedPoints.contains(pixel)) {
            return; //we've already been there.
        } else {
            visitedPoints.add(pixel);
        }

        //check the lower threshold.
        if (pixel.getColor() < lowThreshold) {
            image[pixel.getX()][pixel.getY()] = black;
            return;
        } else {
            image[pixel.getX()][pixel.getY()] = white;
        }

        List<ImagePoint> neighbors = expandNeighbors(pixel, image);

        for (ImagePoint imagePoint : neighbors) {
            //recurse into the neighbors.
            enhanceEdgeNeighbors(imagePoint, image);
        }

    }

    private List<ImagePoint> expandNeighbors(ImagePoint pixel, double[][] image) {

        List<ImagePoint> ret = new LinkedList<ImagePoint>();

        //read the 8 neighbors
        putImagePoint(pixel.getX() - 1, pixel.getY() - 1, image, ret); //upper left
        putImagePoint(pixel.getX(), pixel.getY() - 1, image, ret); //upper
        putImagePoint(pixel.getX() + 1, pixel.getY() - 1, image, ret); //upper right
        putImagePoint(pixel.getX() + 1, pixel.getY(), image, ret); //right
        putImagePoint(pixel.getX() + 1, pixel.getY() + 1, image, ret); //lower right
        putImagePoint(pixel.getX(), pixel.getY() + 1, image, ret); //lower
        putImagePoint(pixel.getX() - 1, pixel.getY() + 1, image, ret); //lower left;
        putImagePoint(pixel.getX() - 1, pixel.getY(), image, ret); //left
        
        return ret;

    }

    private void putImagePoint(int x, int y, double[][] image, List<ImagePoint> ret) {
        int color = -1;

        if (x >= 0 && x < image.length && y >= 0 && y < image[x].length) {
            color = (int) Math.round(image[x][y]);
        }
        
        if (color == 0) {
            //we just dont disturb because it is a useless point.
            return;
        }
        
        ret.add(new ImagePoint(x, y, color));
    }

    enum EdgeDirection {

        HORIZONTAL,
        VERTICAL,
        UNDEFINED,
    }
}
