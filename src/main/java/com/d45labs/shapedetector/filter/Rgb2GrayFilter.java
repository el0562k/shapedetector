package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import java.awt.color.ColorSpace;
import java.awt.image.BufferedImage;
import java.awt.image.BufferedImageOp;
import java.awt.image.ColorConvertOp;
import java.awt.image.ColorModel;

/**
 * converts an rgb image to gray scale.
 * @author osvaldo
 */
public class Rgb2GrayFilter implements Filter {

    private BufferedImageOp grayscaleConv;

    @Override
    public void afterFilter() {
        //do nothing.
    }

    @Override
    public void beforeFilter() {
        grayscaleConv = new ColorConvertOp(ColorSpace.getInstance(ColorSpace.CS_GRAY), null);
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {
        BufferedImage result = grayscaleConv.createCompatibleDestImage(image, ColorModel.getRGBdefault());
        grayscaleConv.filter(image, result);                
        return new BufferedImage[]{result};    
    }
}
