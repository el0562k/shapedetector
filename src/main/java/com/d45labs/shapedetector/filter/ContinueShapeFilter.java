package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import java.awt.image.BufferedImage;

/**
 * this filter modifies our image to generate complete shapes.
 * @author osvaldo
 */
public class ContinueShapeFilter implements Filter {

    @Override
    public void afterFilter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void beforeFilter() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
