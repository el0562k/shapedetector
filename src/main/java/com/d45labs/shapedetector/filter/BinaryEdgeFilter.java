package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import com.d45labs.shapedetector.morphologies.MorphologyOperations;
import java.awt.image.BufferedImage;

/**
 * this filter uses binary operations to solve the border.
 * @author osvaldo
 */
public class BinaryEdgeFilter implements Filter {

    private MorphologyOperations morphologyOperations;
    private boolean[][] se;
    private int iterations;

    public BinaryEdgeFilter(int iterations, int seSize) {
        this.iterations = iterations;
        morphologyOperations = new MorphologyOperations();
        se = morphologyOperations.ones(seSize);
    }

    public BinaryEdgeFilter(int iterations, boolean[][] se) {
        morphologyOperations = new MorphologyOperations();
        this.se = se;
    }

    @Override
    public void afterFilter() {
    }

    @Override
    public void beforeFilter() {
        if(iterations<=0){
            iterations=1;
        }
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {
        boolean[][] A = morphologyOperations.image2array(image);
        boolean[][] erode = morphologyOperations.erode(se, A);
        for (int i = 1; i < iterations; i++) {
            erode = morphologyOperations.erode(se, erode);
        }
        return new BufferedImage[]{morphologyOperations.array2Image(morphologyOperations.substract(A, erode))};
    }
}
