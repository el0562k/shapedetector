package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import java.awt.image.BufferedImage;
import java.awt.image.ConvolveOp;
import java.awt.image.Kernel;

/**
 * this filter just extract edges.
 *
 * @author osvaldo
 */
public class ConvolutionEdgeFilter implements Filter {

    private float[][] operators;
    private final int operatorWidth;

    public ConvolutionEdgeFilter(float[][] operators, int operatorWidth) {
        this.operators = operators;
        this.operatorWidth = operatorWidth;
    }

    @Override
    public void afterFilter() {
    }

    @Override
    public void beforeFilter() {
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {
        BufferedImage[] ret = new BufferedImage[operators.length];

        for (int i = 0; i < operators.length; i++) {
            float[] operator = operators[i];
            //this is a square operator
            Kernel kernel = new Kernel(operatorWidth, operatorWidth, operator);
            ConvolveOp op = new ConvolveOp(kernel);

            ret[i] = op.createCompatibleDestImage(image, image.getColorModel());
            op.filter(image, ret[i]);
        }

        return ret;
    }
}
