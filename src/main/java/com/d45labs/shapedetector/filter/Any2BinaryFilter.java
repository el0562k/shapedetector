package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;

/**
 * gray to binary filter.
 * @author osvaldo
 */
public class Any2BinaryFilter implements Filter {
    public static int FILTER_TRUE = 1;
    public static int FILTER_FALSE= 0;
    private int threshold;

    public Any2BinaryFilter(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public void afterFilter() {
    }

    @Override
    public void beforeFilter() {
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {        
        BufferedImage dest = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_BINARY);
        //        WritableRaster raster = image.getRaster();
        WritableRaster raster = dest.getRaster();
        
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                //                int rGB = image.getRGB(x, y);
                //                int r = (rGB >> 16) & 0xff;
                //                int g = (rGB >> 8) & 0xff;
                //                int b = (rGB) & 0xff;
                Color color = new Color(image.getRGB(x, y));
                
                if(color.getRed()>threshold && color.getGreen()>threshold &&  color.getBlue()>threshold){
                    raster.setSample(x, y, 0, FILTER_TRUE);
                }else{
                    raster.setSample(x, y, 0, FILTER_FALSE);                    
                }                
            }
        }
        
        return new BufferedImage[]{dest};
    }
}
