package com.d45labs.shapedetector.filter;

import com.d45labs.shapedetector.model.filter.Filter;
import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * this filter splits our image in severals color channels and returns a colection of images equal to our color space.
 * by default extracts red green and blue and returns 3 images on gray scale.
 * @author osvaldo
 */
public class ColorChannelFilter implements Filter {

    @Override
    public void afterFilter() {
    }

    @Override
    public void beforeFilter() {
    }

    @Override
    public BufferedImage[] process(BufferedImage image) {
        BufferedImage rchannel = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage gchannel = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
        BufferedImage bchannel = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_BYTE_GRAY);


        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                Color c = new Color(image.getRGB(x, y));

                rchannel.getRaster().setSample(x, y, 0, c.getRed());
                gchannel.getRaster().setSample(x, y, 0, c.getGreen());
                bchannel.getRaster().setSample(x, y, 0, c.getBlue());

            }
        }
        return new BufferedImage[]{rchannel,gchannel,bchannel};
    }
}
