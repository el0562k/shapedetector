package com.d45labs.shapedetector.fourier;

import java.text.DecimalFormat;

/**
 * complexes with error
 * @author osvaldo
 */
public class NativeComplex {

    private double real;
    private double img;

    public NativeComplex() {
    }
    
    public NativeComplex(double real, double img) {
        this.real = real;
        this.img = img;
    }


    public double getImg() {
        return img;
    }

    public void setImg(double img) {
        this.img = img;
    }

    public double getReal() {
        return real;
    }

    public void setReal(double real) {
        this.real = real;
    }
    
    public static NativeComplex euler(double img) {
        NativeComplex nc = new NativeComplex();
        nc.setReal(Math.cos(img));
        nc.setImg(Math.sin(img));
        return nc;
    }
    
    @Override
    public NativeComplex clone(){
        return new NativeComplex(real, img);
    }
    
    public NativeComplex add(NativeComplex complex){
        NativeComplex nativeComplex = new NativeComplex();
        
        nativeComplex.setImg(this.img+complex.getImg());
        nativeComplex.setReal(this.real+complex.getReal());
        return nativeComplex;
        
    }
    
    public NativeComplex mult(NativeComplex c){
        NativeComplex nativeComplex = new NativeComplex();
        nativeComplex.setReal((this.real * c.getReal()) -(this.img * c.getImg()));
        nativeComplex.setImg((this.real*c.getImg()) + (this.img * c.getReal()));        
        return nativeComplex;        
    }
    
    
    public NativeComplex scale(Double scale){
        NativeComplex nativeComplex = new NativeComplex();
        nativeComplex.setImg(this.img*scale);
        nativeComplex.setReal(this.real*scale);
        return nativeComplex;
    }
    /**
     * returns R = sqrt(real^2+img^2)
     * its the value of r coeficient from real and img.
     * @return 
     */
    public double abs(){
        return Math.sqrt(Math.pow(this.real, 2)+Math.pow(this.img, 2));
    }
    /**
     * return Phi = atan2(real,img);
     * its the angle between real part and img part
     * @return 
     */
    public double arg(){
        return Math.atan2(this.real, this.img);
    }

    @Override
    public String toString() {
//        return + real + "+" + img + 'i';
        return  DecimalFormat.getInstance().format(real) + (img>=0?"+":"") + DecimalFormat.getInstance().format(img) + 'i';
    }
    
}
