package com.d45labs.shapedetector.fourier;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * computes an discrete fourier transform using Euler formula
 * @author osvaldo
 */
public final class DFT {

    /**
     * makes a transform from a sample to a complex array.
     * @time O(n^2) where n=sample size.
     * @param samples
     * @return 
     */
    public Complex[] transform(Complex[] samples) {
        Complex[] transform = new Complex[samples.length];
        int N = samples.length;
//        int u = N;
        double omega = (-2 * Math.PI / N);
        for (int k = 0; k < N; k++) {
            Complex sigma = new Complex(0, 0);
            for (int n = 0; n < N; n++) {
                Complex W = Complex.euler(omega * k * n);
                Complex multiply = samples[n].multiply(W);
                sigma = sigma.sum(multiply);
            }
            transform[k] = sigma;
        }

        return transform;
    }
    /**
     * transform to native complex
     * @param samples
     * @return 
     */
    public NativeComplex[] transform(NativeComplex[] samples) {
        NativeComplex[] transform = new NativeComplex[samples.length];
        int N = samples.length;
//        int u = N;
        double omega = (-1 * 2 * Math.PI / N);
        for (int k = 0; k < N; k++) {
            NativeComplex sigma = new NativeComplex(0, 0);
            for (int n = 0; n < N; n++) {
                NativeComplex eW = NativeComplex.euler(omega * k * n);
                NativeComplex mult = samples[n].mult(eW);

                sigma = sigma.add(mult);
            }
            transform[k] = sigma;
        }

        return transform;
    }

    /**
     * computes phase constellation.
     * @param values
     * @return 
     */
    public Double[] computePhi(Complex[] values) {
        Double[] phiArray = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            Complex complex = values[i];
            phiArray[i] = complex.arg().doubleValue();
        }
        return phiArray;
    }

    public NativeComplex[] revert(NativeComplex[] transform) {
        NativeComplex[] samples = new NativeComplex[transform.length];
        int N = transform.length;
//        int u = N;
        double omega = (2 * Math.PI / N);
//        double ri = new BigDecimal(1).divide(BigDecimal.valueOf(N),30,RoundingMode.HALF_EVEN).doubleValue();
        double ri = 1f/N;
        for (int k = 0; k < N; k++) {
            NativeComplex sigma = new NativeComplex(0, 0);
            for (int n = 0; n < N; n++) {
                NativeComplex eW = NativeComplex.euler(omega * k * n);
                NativeComplex mult = transform[n].mult(eW);

                sigma = sigma.add(mult);
            }

            samples[k] = sigma.scale(ri);
        }
        return samples;
    }

    /**
     * compute argument array
     * @param values
     * @return 
     */
    public Double[] computeArgument(Complex[] values) {
        Double[] modArray = new Double[values.length];
        for (int i = 0; i < values.length; i++) {
            Complex complex = values[i];
            modArray[i] = complex.mod().doubleValue();
        }
        return modArray;
    }
}
