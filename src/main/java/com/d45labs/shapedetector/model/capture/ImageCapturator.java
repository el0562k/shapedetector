package com.d45labs.shapedetector.model.capture;

import java.awt.image.BufferedImage;

/**
 * defines an abstract image capturator.
 * @author osvaldo
 */
public interface ImageCapturator {
    /**
     * do something after capture
     */
    void afterCapture();
    /**
     * do something before capture
     */
    void beforeCapture();
    /**
     * compute and get an image
     * @return an captured image.
     */
    BufferedImage getImage();
    /**
     * this stream is continue?
     * @return 
     */
    boolean isContinue();
    /**
     * get number of captures
     * @return number of captures
     */
    int getCaptureSize();
    
    /**
     * process the next image.
     */
    void next();
    
}
