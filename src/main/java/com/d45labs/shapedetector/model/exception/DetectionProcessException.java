package com.d45labs.shapedetector.model.exception;

/**
 *
 * @author osvaldo
 */
public class DetectionProcessException extends Exception {
    private static final long serialVersionUID = 1L;

    public DetectionProcessException(int code, Throwable cause) {
        super("there are a problem during detection process", cause); // super hardcoded!
    }
    
}
