package com.d45labs.shapedetector.model.feature;

import java.awt.image.BufferedImage;

/**
 * compute features using any method.
 * @author osvaldo
 */
public interface FeatureGetter<T> {
    /**
     * before computation
     */
    void before();
    /**
     * after computation
     */    
    void after();
     /**
     * computes an feature adapter.
     * @param image
     * @return 
     */
    FeatureAdapter<T> computeFeatures(BufferedImage image);
}
