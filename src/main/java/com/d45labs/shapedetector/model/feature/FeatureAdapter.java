package com.d45labs.shapedetector.model.feature;

import java.util.List;

/**
 * adapts different features.
 * @author osvaldo
 */
public interface FeatureAdapter<T> {
    /**
     * before adapt
     */
    void before();
    /**
     * after adapt
     */
    void after();
    /**
     * adapts this features
     * @return list of features.
     */
    List<Feature<T>> adapt();
    /**
     * get the feature adapter name.
     * @return 
     */
    String getName();
    
    
}
