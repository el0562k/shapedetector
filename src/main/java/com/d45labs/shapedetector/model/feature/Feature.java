package com.d45labs.shapedetector.model.feature;

/**
 * a kind of feature of 
 * @author osvaldo
 */
public interface Feature<T> {
 
    T get();
    void set(T t);
    boolean equals(Feature<T> t);
    
}
