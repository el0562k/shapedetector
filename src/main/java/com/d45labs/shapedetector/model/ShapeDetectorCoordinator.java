package com.d45labs.shapedetector.model;

import com.d45labs.shapedetector.model.capture.ImageCapturator;
import com.d45labs.shapedetector.model.filter.Filter;
import com.d45labs.shapedetector.model.classifier.Classifier;
import com.d45labs.shapedetector.model.classifier.ClassifierResultAdapter;
import com.d45labs.shapedetector.model.classifier.Result;
import com.d45labs.shapedetector.model.exception.DetectionProcessException;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;
import com.d45labs.shapedetector.model.feature.FeatureGetter;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * coordination of efforts of detection.
 * @author osvaldo
 */
public class ShapeDetectorCoordinator {

    private ImageCapturator capturator;
    private List<Filter> filters;
    private FeatureGetter featureGetter;
    private Classifier classifier;

    public ShapeDetectorCoordinator() {
        this.filters = new ArrayList<Filter>();
    }

    /**
     * attachs an image capturator.
     * @param capturator 
     */
    public void attachImageCapturator(ImageCapturator capturator) {
        this.capturator = capturator;
    }

    /**
     * attach an filter to do image pre-processing.
     * @param filter 
     */
    public void attachFilter(Filter[] filter) {
        this.filters.addAll(Arrays.asList(filter));
    }

    /**
     * sets an feature getter
     * @param featureGetter 
     */
    public void attachFeatureGetter(FeatureGetter featureGetter) {
        this.featureGetter = featureGetter;
    }

    /**
     * attachs a classifier.
     * @param classifier 
     */
    public void attachClassifier(Classifier classifier) {
        this.classifier = classifier;
    }

    /**
     * try to detect something from a single snapshot..
     * @return 
     */
    public List<Result> detect() throws DetectionProcessException {
        validateStatus();
        BufferedImage image;
        //first get an image.
        try {
            this.capturator.beforeCapture();
            image = this.capturator.getImage();
            this.capturator.afterCapture();
        } catch (Exception e) {
            throw new DetectionProcessException(-2, e);
        }
        //next lets do a filter if applies
        LinkedList<BufferedImage> cache = new LinkedList<BufferedImage>();
        try {
            cache.push(image);
            if (!this.filters.isEmpty()) {
                for (Filter filter : filters) {
                    List<BufferedImage> list = new ArrayList<BufferedImage>();
                    Collections.copy(list, cache);
                    filter.beforeFilter();
                    for (BufferedImage toFilter : list) {
                        BufferedImage[] process = filter.process(toFilter);
                        for (BufferedImage filtered : process) {
                            cache.push(filtered);
                        }
                        cache.remove(toFilter);
                    }
                    filter.afterFilter();
                }
            }
        } catch (Exception e) {
            throw new DetectionProcessException(-4, e);
        }
        //we have a lot of images processed. we have to extract features from this processed images.
        //next extract features
        List<FeatureAdapter> features = new ArrayList<FeatureAdapter>();
        try {
            this.featureGetter.before();
            for (BufferedImage cachedImage : cache) {
                features.add(this.featureGetter.computeFeatures(cachedImage));
            }
            this.featureGetter.after();
        } catch (Exception e) {
            throw new DetectionProcessException(-8, e);
        }

        //next classify
        List<ClassifierResultAdapter> resultAdapterList = new ArrayList<ClassifierResultAdapter>();
        try {
            this.classifier.before();
            for (FeatureAdapter featureAdapter : features) {
                featureAdapter.before();
                resultAdapterList.add(this.classifier.classify(featureAdapter));
                featureAdapter.adapt();
            }
            this.classifier.after();
        } catch (Exception e) {
            throw new DetectionProcessException(-16, e);
        }
        //finally compute results.

        List<Result> results = new ArrayList<Result>();
        try {
            for (ClassifierResultAdapter classifierResultAdapter : resultAdapterList) {
                classifierResultAdapter.before();
                results.addAll(classifierResultAdapter.adapt());
                classifierResultAdapter.after();
            }
        } catch (Exception e) {
            throw new DetectionProcessException(-32, e);
        }

        return results;
    }

    private void validateStatus() throws DetectionProcessException {
        if (this.capturator == null && featureGetter == null && classifier == null) {
            throw new DetectionProcessException(-1 ,null);
        }
    }
}
