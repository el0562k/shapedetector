package com.d45labs.shapedetector.model.filter;

import java.awt.image.BufferedImage;

/**
 * Interface that defines an image filter 
 * @author osvaldo
 */
public interface Filter {
    /**
     * do something after filter.
     */
    void afterFilter();
    /**
     * do something before filter
     */
    void beforeFilter();
    /**
     * 
     * @param image
     * @return an vector of BufferedImages. we can think on image channels.
     */
    BufferedImage[] process(BufferedImage image);
            
}
