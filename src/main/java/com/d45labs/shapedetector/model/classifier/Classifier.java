package com.d45labs.shapedetector.model.classifier;

import com.d45labs.shapedetector.model.feature.FeatureAdapter;

/**
 * defines an classifier.
 * @author osvaldo
 */
public interface Classifier {
    /**
     * before classify
     */
    void before();
    /**
     * after classify
     */
    void after();
    /**
     * executes an classification.
     * @param features
     * @return 
     */
    ClassifierResultAdapter classify(FeatureAdapter features);
    
    
}
