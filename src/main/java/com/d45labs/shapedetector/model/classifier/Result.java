package com.d45labs.shapedetector.model.classifier;

/**
 *
 * @author osvaldo
 */
public interface Result<T> {
    
    T get();
    void set(T t);
        
    @Override
    String toString();
}
