package com.d45labs.shapedetector.model.classifier;

import java.util.List;

/**
 *
 * @author osvaldo
 */
public interface ClassifierResultAdapter {
    /**
     * before adapt
     */
    void before();
    /**
     * after adapt
     */
    void after();
    /**
     * compute results.
     * @return 
     */
    List<Result>adapt();
}
