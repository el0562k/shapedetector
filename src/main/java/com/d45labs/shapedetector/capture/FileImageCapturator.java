package com.d45labs.shapedetector.capture;

import com.d45labs.shapedetector.model.capture.ImageCapturator;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;

/**
 * captures an image from a file.
 * @author osvaldo
 */
public class FileImageCapturator implements ImageCapturator {

    private String filename;

    public FileImageCapturator(String filename) {
        this.filename = filename;
    }

    @Override
    public void afterCapture() {
        Logger.getLogger(FileImageCapturator.class.getName()).log(Level.INFO, "Image readed sucessfully.");
    }

    @Override
    public void beforeCapture() {
        Logger.getLogger(FileImageCapturator.class.getName()).log(Level.INFO, "Starting of read an image from a file: {0}", filename);
    }

    @Override
    public BufferedImage getImage() {
        try {
            return ImageIO.read(new File(filename));
        } catch (IOException ex) {
            Logger.getLogger(FileImageCapturator.class.getName()).log(Level.SEVERE, "something wrong during file data access.", ex);
        } catch (Exception ex){
            Logger.getLogger(FileImageCapturator.class.getName()).log(Level.SEVERE, "i cant read file", ex);
        }
        return null;
    }

    @Override
    public boolean isContinue() {
        return false;
    }

    @Override
    public int getCaptureSize() {
        return 1;
    }

    @Override
    public void next() {
        throw new UnsupportedOperationException("Not supported.");
    }
}
