package com.d45labs.shapedetector.classifier;

import com.d45labs.shapedetector.model.classifier.Classifier;
import com.d45labs.shapedetector.model.classifier.ClassifierResultAdapter;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;

/**
 * this classifier is a implementation of a hopfield neural net (feedbacked perceptron)
 * @author osvaldo
 */
public class HopfieldNeuralNetClassifier implements Classifier {

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ClassifierResultAdapter classify(FeatureAdapter features) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
