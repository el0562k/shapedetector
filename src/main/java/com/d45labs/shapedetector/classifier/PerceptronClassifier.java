package com.d45labs.shapedetector.classifier;

import com.d45labs.shapedetector.model.classifier.Classifier;
import com.d45labs.shapedetector.model.classifier.ClassifierResultAdapter;
import com.d45labs.shapedetector.model.feature.FeatureAdapter;

/**
 * just an perceptron.
 * @author osvaldo
 */
public class PerceptronClassifier implements Classifier {

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public ClassifierResultAdapter classify(FeatureAdapter features) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
}
