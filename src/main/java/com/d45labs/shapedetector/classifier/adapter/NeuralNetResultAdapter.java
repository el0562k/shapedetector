package com.d45labs.shapedetector.classifier.adapter;

import com.d45labs.shapedetector.model.classifier.ClassifierResultAdapter;
import com.d45labs.shapedetector.model.classifier.Result;
import java.util.List;

/**
 *
 * @author osvaldo
 */
public class NeuralNetResultAdapter implements ClassifierResultAdapter{

    @Override
    public void before() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void after() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<Result> adapt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
}
