package com.d45labs.shapedetector.morphologies;

import com.d45labs.shapedetector.utils.ImageIterator;
import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * applies morphologies operations to binary images
 * @author osvaldo
 */
public final class MorphologyOperations {

    public MorphologyOperations() {
    }

    /**
     * check if this image has BufferedImage.TYPE_BYTE_BINARY as type.
     * @param image
     * @return 
     */
    private boolean checkImage(BufferedImage image) {
        return image != null && image.getType() == BufferedImage.TYPE_BYTE_BINARY;
    }

    public boolean[][] image2array(BufferedImage image) {
        if (checkImage(image)) {
            final boolean[][] array = new boolean[image.getWidth()][image.getHeight()];
            final int white = Color.white.getRGB();
            final int black = Color.black.getRGB();
            new ImageIterator(image) {

                @Override
                protected void onElement(int x, int y, Color color) {
                    array[x][y] = (color.getRGB() == black ? true : false);
                }
            }.iterate();
            return array;
        } else {
            return null;
        }
    }

    /**
     * generate an image from boolean bi-dimensional array
     * @param array
     * @return an image as BufferedImage.TYPE_BYTE_BINARY
     */
    public BufferedImage array2Image(final boolean[][] array) {
        final BufferedImage image = new BufferedImage(array.length, array[0].length, BufferedImage.TYPE_BYTE_BINARY);
        final int rGBlack = Color.black.getRGB();
        final int rGBwhite = Color.WHITE.getRGB();//optimize object creation.
        new ImageIterator(image) {

            @Override
            protected void onElement(int x, int y, Color color) {
                image.getRaster().setSample(x, y, 0, (array[x][y] == true ? rGBlack : rGBwhite));
            }
        }.iterate();
        return image;
    }

    /**
     * makes a 180 degree rotation of our matrix
     * @param source
     * @return 
     */
    public boolean[][] reflects(boolean[][] source) {
        boolean[][] generateNew = this.generateNew(source);
        for (int i = 0, ir = source.length - 1; i < source.length; i++, ir--) {
            for (int j = 0, jr = source[0].length - 1; j < source[0].length; j++, jr--) {

                generateNew[i][j] = source[ir][jr];
            }
        }
        return generateNew;
    }

    /**
     * 
     * @param x
     * @param y
     * @param A
     * @param rootx set
     * @param rooty
     * @param se
     * @return 
     */
    public boolean[][] translate(int x, int y, boolean[][] A, boolean[][] se) {
        int dx = (se.length / 2);
        int dy = (se[0].length / 2);
        boolean[][] generateNew = this.generateNew(A);
        for (int i = 0, ix = x; i < se.length; i++, ix++) {
            for (int j = 0, jy = y; j < se[0].length; j++, jy++) {
                int refx = ix - dx;
                int refy = jy - dy;
                if (refx >= 0 && refx < generateNew.length && refy >= 0 && refy < generateNew[0].length) {
                    generateNew[refx][refy] = se[i][j];
                }
            }
        }
        return generateNew;
    }

    /**
     * complements an binary image..
     * @param source
     * @return 
     */
    public boolean[][] complement(boolean[][] source) {
        boolean[][] generateNew = this.generateNew(source);
        for (int i = 0; i < source.length; i++) {
            for (int j = 0; j < source[0].length; j++) {
                generateNew[i][j] = !source[i][j];
            }
        }
        return generateNew;
    }

    /**
     * A C B = A - B={x| xEA,x!EB} (E=belongs, !E=Not belongs)
     * @param A
     * @param B
     * @return 
     */
    public boolean[][] substract(boolean[][] A, boolean[][] B) {
        if (checkSameDimension(A, B)) {
            return intersects(A, complement(B));
        } else {
            return null;
        }
    }

    /**
     * A U B = {x| xEA or xEB}
     * @param A
     * @param B
     * @return 
     */
    public boolean[][] union(boolean[][] A, boolean[][] B) {
        if (checkSameDimension(A, B)) {
            boolean[][] generateNew = this.generateNew(A);
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A[0].length; j++) {
                    generateNew[i][j] = A[i][j] | B[i][j];
                }
            }
            return generateNew;
        } else {
            return null;
        }
    }

    /**
     * A n B = {x| xEA and xEB}
     * @param A
     * @param B
     * @return 
     */
    public boolean[][] intersects(boolean[][] A, boolean[][] B) {
        if (checkSameDimension(A, B)) {
            boolean[][] generateNew = this.generateNew(A);
            for (int i = 0; i < A.length; i++) {
                for (int j = 0; j < A[0].length; j++) {
                    generateNew[i][j] = A[i][j] & B[i][j];
                }
            }
            return generateNew;
        } else {
            return null;
        }
    }

    /**
     * creates a new array with the same dimension.
     * @param source
     * @return 
     */
    private boolean[][] generateNew(boolean[][] source) {
        return new boolean[source.length][source[0].length];
    }

    /**
     * A has the same dimension than B?
     * @param A
     * @param B
     * @return 
     */
    private boolean checkSameDimension(boolean[][] A, boolean[][] B) {
        return A.length == B.length && A[0].length == B[0].length;
    }

    /**
     * dilates A
     * @param se
     * @param A
     * @return 
     */
    public boolean[][] dilate(boolean[][] se, boolean[][] A) {
        boolean[][] rse = this.reflects(se);
        boolean[][] generateNew = this.generateNew(A);
        for (int i = 0; i < generateNew.length; i++) {
            for (int j = 0; j < generateNew[0].length; j++) {
                boolean[][] translate = this.translate(i, j, A, rse);
                boolean[][] intersects = this.intersects(translate, A);
                if (!isEmpty(intersects)) {
                    generateNew[i][j] = true;
                }
            }
        }
        return generateNew;
    }

    /**
     * erodes A
     * @param se
     * @param A
     * @return 
     */
    public boolean[][] erode(boolean[][] se, boolean[][] A) {
        //complement
        boolean[][] Ac = this.borderize(se, A);
        //add border of SE dimension.
        boolean[][] AcBorderized = this.complement(Ac);
        //generates an stable eroded A
        boolean[][] generateNew = this.generateNew(A);

        int shiftx = se.length / 2;
        int shifty = se[0].length / 2;


        for (int i = shiftx, ii = 0; i < Ac.length; i++, ii++) {
            for (int j = shifty, jj = 0; j < Ac[0].length; j++, jj++) {
                boolean[][] translate = this.translate(i, j, AcBorderized, se);
                boolean[][] intersects = this.intersects(translate, AcBorderized);
                if (isEmpty(intersects)) {
                    generateNew[ii][jj] = true;
                }
            }
        }


//        
//        for (int i = 0; i < Ac.length; i++) {
//            for (int j = 0; j < Ac[0].length; j++) {
//                boolean[][] translate = this.translate(i, j, Ac, se);
//                boolean[][] intersects = this.intersects(translate, Ac);
//                if (isEmpty(intersects)) {
//                    generateNew[i][j] = true;
//                }
//            }
//        }
        return generateNew;
    }

    /**
     *  safe subspace extraction.
     * @param x from x
     * @param y from x
     * @param deltax how many elements i've to get
     * @param deltay how many elements i've to get
     * @param A image source.
     * @return 
     */
    public boolean[][] subspace(int x, int y, int deltax, int deltay, boolean[][] A) {
        boolean[][] subspace = new boolean[deltax][deltay];
        for (int i = 0; i < deltax; i++) {
            for (int j = 0; j < deltay; j++) {
                //boundary check
                if (x + i < A.length && y + j < A[0].length) {
                    subspace[i][j] = A[x + i][y + j];
                }
            }
        }
        return subspace;
    }

    /**
     * safe subspace merge
     * @param x pivot point x it should be positive
     * @param y pivot point y it should be positive
     * @param subspace subspace source
     * @param A universe destiny
     */
    public boolean[][] mergeSubspaceAt(int x, int y, boolean[][] subspace, boolean[][] A) {
        for (int i = x, is = 0; is < subspace.length; i++, is++) {
            for (int j = y, js = 0; js < subspace[0].length; j++, js++) {
//                if (!(i + subspace.length > A.length) && (j + subspace[0].length > A[0].length)) { //boundary check
                if ((i < (x + subspace.length) && j < (y + subspace[0].length))) {
                    A[i][j] = subspace[is][js];
                }
//                }
            }
        }
        return A;
    }

    private boolean isEmpty(boolean[][] set) {
        for (int i = 0; i < set.length; i++) {
            for (int j = 0; j < set[0].length; j++) {
                if (set[i][j] == true) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * add borders of se dimension to A. this is maybe an expensive operation.
     * @param se
     * @param A
     * @return 
     */
    public boolean[][] borderize(boolean[][] se, boolean[][] A) {
        int lx = se.length / 2;
        int ly = se[0].length / 2;
        boolean[][] result = new boolean[A.length + lx * 2][A[0].length + ly * 2];
        int shiftx = se.length / 2;
        int shifty = se[0].length / 2;

        return this.mergeSubspaceAt(shiftx, shifty, A, result);
    }

    /**
     * creates an symmetric array of ones.
     * @param dim
     * @return 
     */
    public boolean[][] ones(int dim) {
        boolean[][] ones = new boolean[dim][dim];
        for (int i = 0; i < ones.length; i++) {
            for (int j = 0; j < ones.length; j++) {
                ones[i][j] = true;
            }
        }
        return ones;
    }
    /**
     * creates an symmetric array of zeros.
     * @param dim
     * @return 
     */
    public boolean[][] zeros(int dim) {
        return new boolean[dim][dim];
    }
}
