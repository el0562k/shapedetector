package com.d45labs.shapedetector.utils;

import java.awt.Color;
import java.awt.image.BufferedImage;

/**
 * iterates an image over each element.
 * @author osvaldo
 */
public abstract class ImageIterator {

    BufferedImage image;

    public ImageIterator(BufferedImage image) {
        this.image = image;
    }

    public void iterate(){        
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                //                int rGB = image.getRGB(x, y);
                //                int r = (rGB >> 16) & 0xff;
                //                int g = (rGB >> 8) & 0xff;
                //                int b = (rGB) & 0xff;
                Color color = new Color(image.getRGB(x, y));                                
                this.onElement(x, y, color);
            }
        }
    }
    
    protected abstract void onElement(int x, int y, Color color);
}
