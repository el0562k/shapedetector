package com.d45labs.shapedetector.utils;

import java.io.Serializable;

/**
 * Immutable abstraction of a point.
 * @author juancavallotti
 */
public class Point implements Serializable {
    private static final long serialVersionUID = 1L;
    protected final int x;
    protected final int y;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Point other = (Point) obj;
        if (this.x != other.x) {
            return false;
        }
        if (this.y != other.y) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + this.x;
        hash = 97 * hash + this.y;
        return hash;
    }

    @Override
    public String toString() {
        return "Point{" + "x=" + x + ", y=" + y + '}';
    }
    
    /**
     * Check if two points are adjacent.
     * @param p
     * @return 
     */
    public boolean isAdjacentToPoint(Point p) {
        
        if (p == null) {
            return false;
        }
        
        int absDifX = Math.abs(this.x - p.x);
        int absDifY = Math.abs(this.y - p.y);
        
        return absDifX <= 1 && absDifY <= 1 && absDifX + absDifY > 0;
    }
    
}
