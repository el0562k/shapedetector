package com.d45labs.shapedetector.utils;

/**
 * Abstraction of a point on an image.
 * @author juancavallotti
 */
public class ImagePoint extends Point {
    private static final long serialVersionUID = 1L;
    
    private final int color;

    public ImagePoint(int x, int y, int color) {
        super(x, y);
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ImagePoint other = (ImagePoint) obj;
        
        if (!super.equals(obj)) {
            return false;
        }
        
        if (this.color != other.color) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * super.hashCode();
        hash = 41 * hash + this.color;
        return hash;
    }

    @Override
    public String toString() {
        return "ImagePoint{" + "x=" + getX() + ", y=" + getY() + "color=" + color + '}';
    }
    
    
}
