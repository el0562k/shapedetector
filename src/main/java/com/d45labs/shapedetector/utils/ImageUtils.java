package com.d45labs.shapedetector.utils;

import java.awt.image.BufferedImage;

/**
 * Common Utility functions for image processing.
 *
 * @author juancavallotti
 */
public class ImageUtils {

    /**
     * Utility method for safely reading data from a pixel
     *
     * @param x
     * @param y
     * @param image
     * @return
     */
    public static int safeReadPixelValue(int x, int y, BufferedImage image) {
        if (x < 0 || x >= image.getWidth()) {
            return -1;
        }

        if (y < 0 || y >= image.getHeight()) {
            return -1;
        }

        return image.getRGB(x, y);
    }

    /**
     * Returns the color level for a pixel on a grayscale image.
     *
     * @param x
     * @param y
     * @param img
     * @return
     */
    public static int safeReadGrayPixel(int x, int y, BufferedImage img) {
        return safeReadPixelValue(x, y, img) & 0xFF;
    }

    /**
     * Convert an array of doubles to a grayscale image.
     * @param image
     * @return 
     */
    public static BufferedImage toGrayImage(double[][] image) {
        return toGrayImage(image, 0.0);
    }
    
    /**
     * Convert an array of doubles to a grayscale image.
     * @param image
     * @return 
     */
    public static BufferedImage toGrayImage(double[][] image, double offset) {

        BufferedImage ret = new BufferedImage(image.length, image[0].length, BufferedImage.TYPE_BYTE_GRAY);

        for (int i = 0; i < image.length; i++) {
            double[] row = image[i];
            for (int j = 0; j < row.length; j++) {
                double level = row[j];
                ret.setRGB(i, j, grayLevelToColor(offset + level));
            }
        }
        return ret;
    }

    /**
     * Convert a gray level to color.
     *
     * @param level
     * @return
     */
    public static int grayLevelToColor(double level) {
        long number = Math.round(level);
        
        if (number > 255) {
            number = 255;
        }
        
        if (number < 0) {
            number = 0;
        }
        
        long ret = 0xFF;
        ret = (ret << 8) | number;
        ret = (ret << 8) | number;
        ret = (ret << 8) | number;
        
        
        return (int) ret;
    }
}
