package com.d45labs.shapedetector.utils;

import java.awt.image.BufferedImage;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/**
 * Utility class with common operation over images.
 *
 * @author juancavallotti
 */
public class ImageOperations {

    /**
     * Standard convolution process, the returned value may have negative
     * values.
     *
     * @param img the image to convolve
     * @param kernel the kernel.
     * @return a matrix with the result of the convolution unaltered.
     */
    public static double[][] convolveImage(BufferedImage img, double[][] kernel) {
        double[][] ret = new double[img.getWidth()][img.getHeight()];

        int kRows = kernel[0].length;
        int kCols = kernel.length;

        int kCenterCols = kCols / 2;
        int kCenterRows = kRows / 2;



        for (int i = 0; i < img.getWidth(); i++) {
            for (int j = 0; j < img.getHeight(); j++) {
                for (int m = 0; m < kRows; m++) {
                    int mm = kRows - 1 - m;

                    for (int n = 0; n < kCols; n++) {
                        int nn = kCols - 1 - n;

                        int ii = i + (m - kCenterRows);
                        int jj = j + (n - kCenterCols);

                        if (ii >= 0 && ii < img.getWidth() && jj >= 0 && jj < img.getHeight()) {
                            int pixelLevel = ImageUtils.safeReadGrayPixel(ii, jj, img);
                            ret[i][j] +=  pixelLevel * kernel[nn][mm];
                        }
                        
                    } //end n for
                    
                } //end m for
                //Logger.getLogger(ImageOperations.class.getName()).log(Level.INFO, String.format("i: %d j: %d val: %f", i, j, ret[i][j]));
            } //end j for
            
        } // end i for
        return ret;
    }
    
    
    public static void fixSimpleDiscontinuities(BufferedImage img) {
        for (int i = 0; i < img.getWidth(); i++) {
            for (int j = 0; j < img.getHeight(); j++) {
                int level = ImageUtils.safeReadGrayPixel(i, j, img);
                if (level == 0) {
                    Set<Point> nhs = expandNeighbors(new Point(i, j), 1, img);
                    if (nhs.size() != 2) {
                        continue;
                    }
                    Iterator<Point> it = nhs.iterator();
                    Point p1 = it.next();
                    Point p2 = it.next();
                    if (!p1.isAdjacentToPoint(p2)) {
                        img.setRGB(i, j, 0xFFFFFFFF);
                    }
                }
            }
        }
    }
    
    public static Set<Point> expandNeighbors(Point p, int minLevel, BufferedImage img) {
        
        int minXBound = p.getX() - 1;
        int maxXBound = p.getX() + 1;
        
        int minYBound = p.getY() - 1; 
        int maxYBound = p.getY() + 1; 
        
        if (minXBound < 0) {
            minXBound = 0;
        }
        
        if (maxXBound >= img.getWidth()) {
            maxXBound = img.getWidth() - 1;
        }
        
        if (minYBound < 0) {
            minYBound = 0;
        }
        
        if (maxYBound >= img.getHeight()) {
            maxYBound = img.getHeight() - 1;
        }
        
        Set<Point> ret = new HashSet<Point>();
        
        for (int i = minXBound; i <= maxXBound; i++) {
            for (int j = minYBound; j <= maxYBound; j++) {
                
                int color = ImageUtils.safeReadGrayPixel(i, j, img);
                if (color < minLevel) {
                    continue;
                }
                
                if (i == p.getX() && j == p.getX()) {
                    continue; //we dont want the neighbors
                }
                
                Point n = new Point(i, j);
                ret.add(n);
            }
        }
        
        return ret;
    }
}
